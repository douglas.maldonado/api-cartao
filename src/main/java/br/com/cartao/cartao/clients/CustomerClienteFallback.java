package br.com.cartao.cartao.clients;

import br.com.cartao.cartao.exceptions.CustomerOffline;
import br.com.cartao.cartao.models.Customer;

public class CustomerClienteFallback implements CustomerClient{

    @Override
    public Customer getCustomerById(Long id){
        //configurar resposta ou ações a tomar

        throw new CustomerOffline();

//        return null;
    }
}
