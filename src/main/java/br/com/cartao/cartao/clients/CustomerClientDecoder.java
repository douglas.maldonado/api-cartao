package br.com.cartao.cartao.clients;

import br.com.cartao.cartao.exceptions.CustomerNotFound;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomerClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new CustomerNotFound();
        }else{
            return errorDecoder.decode(s, response);
        }

    }

}
