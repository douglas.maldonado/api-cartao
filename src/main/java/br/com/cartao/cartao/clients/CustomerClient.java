package br.com.cartao.cartao.clients;

import br.com.cartao.cartao.exceptions.CustomerOffline;
import br.com.cartao.cartao.models.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "customer", configuration = CustomerClientConfiguration.class) //ligar configuracao
@FeignClient(name = "customer")
public interface CustomerClient {

    @GetMapping("/cliente/{id}")
    Customer getCustomerById(@PathVariable Long id);
}
