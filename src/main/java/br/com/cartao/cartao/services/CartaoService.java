package br.com.cartao.cartao.services;

import br.com.cartao.cartao.clients.CustomerClient;
import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.Customer;
import br.com.cartao.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private CustomerClient customerClient;

    public Cartao salvarCartao(Cartao cartao){

        Customer customer = customerClient.getCustomerById(cartao.getCostumer_id());

        cartao.setCostumer_id(customer.getId());
        cartao.setAtivo(false);

        Cartao cartaoObjeto = cartaoRepository.save(cartao);
        return cartaoObjeto;
    }

    public Cartao ativarCartao(Long numero, boolean ativo){
        Cartao cartaoObjeto = buscarCartaoPorNumero(numero);
        cartaoObjeto.setAtivo(ativo);

        Cartao cartaoRetorno = cartaoRepository.save(cartaoObjeto);

        return cartaoRetorno;
    }

    public Cartao buscarCartaoPorNumero(Long numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if(cartaoOptional.isPresent()){
            return cartaoOptional.get();
        }

        throw new RuntimeException("Cartão não cadastrado");
    }

    public Cartao buscarCartaoPorId(Long id) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if(cartaoOptional.isPresent()){
            return cartaoOptional.get();
        }

        throw new RuntimeException("Cartão não cadastrado");
    }






}
