package br.com.cartao.cartao.models;

public class Customer {

        private Long id;

        private String nome;

        public Customer() {
        }

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getNome() {
                return nome;
        }

        public void setNome(String nome) {
                this.nome = nome;
        }
}
