package br.com.cartao.cartao.models.dtos;

public class EntradaDTOCartao {
    private Long numero;
    private Long clienteId;

    public EntradaDTOCartao() {
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }
}
