package br.com.cartao.cartao.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Numero do Cartão não pode ser nulo")
    private Long numero;

    @NotNull(message = "Cliente não pode ser nulo")
    private Long costumer_id;

    private boolean ativo;

    public Cartao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public Long getCostumer_id() {
        return costumer_id;
    }

    public void setCostumer_id(Long costumer_id) {
        this.costumer_id = costumer_id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
