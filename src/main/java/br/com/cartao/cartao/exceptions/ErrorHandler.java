package br.com.cartao.cartao.exceptions;

import br.com.cartao.cartao.clients.CustomerClienteFallback;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.FeignException;
import feign.RetryableException;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.json.JSONException;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(FeignException.class)
    public ResponseEntity handleFeignException(FeignException e, HttpServletResponse response)
            throws JSONException, IOException {

        HashMap<String,Object> result =
                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);

        return ResponseEntity.status(e.status()).body(result);
    }

//    @ExceptionHandler(RetryableException.class)
//    public ResponseEntity handleRetryableException(RetryableException e, HttpServletResponse response)
//            throws JsonProcessingException {
//
//        HashMap<String,Object> result =
//                (HashMap<String, Object>) new ObjectMapper().readValue(e.contentUTF8(),Map.class);
//
//        return ResponseEntity.status(e.status()).body(result);
//    }

    @Bean
    @ExceptionHandler(RetryableException.class)
    public Feign.Builder builder() {
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new CustomerClienteFallback(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }
}
