package br.com.cartao.cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Serviço Cliente offline.")
public class CustomerOffline extends RuntimeException{
}
