package br.com.cartao.cartao.controllers;

import br.com.cartao.cartao.models.Cartao;
import br.com.cartao.cartao.models.dtos.EntradaDTOCartao;
import br.com.cartao.cartao.models.dtos.RespostaDTOCartao;
import br.com.cartao.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaDTOCartao cadastrarCartao(@RequestBody @Valid EntradaDTOCartao entradaDTOCartao){
        Cartao cartao = new Cartao();

        cartao.setCostumer_id(entradaDTOCartao.getClienteId());
        cartao.setNumero(entradaDTOCartao.getNumero());

        Cartao cartaoObjeto = cartaoService.salvarCartao(cartao);

        RespostaDTOCartao respostaDTOCartao = new RespostaDTOCartao();
        respostaDTOCartao.setId(cartaoObjeto.getId());
        respostaDTOCartao.setNumero(cartaoObjeto.getNumero());
        respostaDTOCartao.setClienteId(cartaoObjeto.getCostumer_id());
        respostaDTOCartao.setAtivo(cartaoObjeto.isAtivo());

        return respostaDTOCartao;
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public RespostaDTOCartao ativarCartao(@PathVariable(name = "numero") Long numero, @RequestBody Cartao cartao){
        Cartao cartaoObjeto = cartaoService.ativarCartao(numero, cartao.isAtivo());

        RespostaDTOCartao respostaDTOCartao = new RespostaDTOCartao();
        respostaDTOCartao.setId(cartaoObjeto.getId());
        respostaDTOCartao.setNumero(cartaoObjeto.getNumero());
        respostaDTOCartao.setClienteId(cartaoObjeto.getCostumer_id());
        respostaDTOCartao.setAtivo(cartaoObjeto.isAtivo());

        return respostaDTOCartao;
    }

    @GetMapping("/{numero}")
    public RespostaDTOCartao consultarCartao(@PathVariable(name = "numero") Long numero){
        Cartao cartaoObjeto = cartaoService.buscarCartaoPorNumero(numero);

        RespostaDTOCartao respostaDTOCartao = new RespostaDTOCartao();
        respostaDTOCartao.setId(cartaoObjeto.getId());
        respostaDTOCartao.setNumero(cartaoObjeto.getNumero());
        respostaDTOCartao.setClienteId(cartaoObjeto.getCostumer_id());
        respostaDTOCartao.setAtivo(cartaoObjeto.isAtivo());

        return respostaDTOCartao;
    }

    @GetMapping("/id/{id}")
    public RespostaDTOCartao consultarCartaoPorId(@PathVariable(name = "id") Long id){
        Cartao cartaoObjeto = cartaoService.buscarCartaoPorId(id);

        RespostaDTOCartao respostaDTOCartao = new RespostaDTOCartao();
        respostaDTOCartao.setId(cartaoObjeto.getId());
        respostaDTOCartao.setNumero(cartaoObjeto.getNumero());
        respostaDTOCartao.setClienteId(cartaoObjeto.getCostumer_id());
        respostaDTOCartao.setAtivo(cartaoObjeto.isAtivo());

        return respostaDTOCartao;
    }


}
